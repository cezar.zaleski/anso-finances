import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
  public nome;
  constructor(public navCtrl: NavController) {
    this.nome =  'Hello World';
  }

  getNome(){
    return "Retornado pelo método: "+ this.nome;
  }

}
