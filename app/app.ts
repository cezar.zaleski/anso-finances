import { Component } from '@angular/core';
import { ionicBootstrap, Platform } from 'ionic-angular';
import { StatusBar } from 'ionic-native';

import { HomePage } from './pages/home/home';
import { ContasPage } from './pages/contas/contas';


@Component({
  templateUrl: 'build/app.html'
})
export class MyApp {
  //rootPage: any = HomePage;
  raiz: any = ContasPage;
  public home = HomePage;
  public contas = ContasPage;

  constructor(platform: Platform) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }
  openPage(opcao){
    this.raiz = opcao;
  }
}

ionicBootstrap(MyApp);
